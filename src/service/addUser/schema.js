module.exports = {
  type: "object",
  additionalProperties: false,
  required: ["username", "email", "birthdate"],
  properties: {
    username: {
      type: "string",
      maxLength: 10,
      errorMessage:
        '"username" should be string with maximum 10 character length',
    },
    email: {
      type: "string",
      maxLength: 256,
      format: "email",
      errorMessage: {
        maxLength: "must NOT have more than 256 characters",
      },
    },
    birthdate: {
      type: "string",
      format: "date",
    },
  },
  errorMessage: {
    required: {
      username: '"username" is required',
      email: '"email" is required',
      birthdate: '"birthdate" is required',
    },
    additionalProperties: "Invalid property",
  },
};
