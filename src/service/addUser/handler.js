const { v4 } = require("uuid");
const AWS = require("aws-sdk");
const payloadValidator = require("../../helpers/payloadValidator");
const response = require("../../helpers/response");
// const middy = require("@middy/core");
// const httpJsonBodyParser = require("@middy/http-json-body-parser");

const schema = require("./schema");

module.exports.addUser = async (event) => {
  try {
    const isPayloadValid = payloadValidator(schema, event.body);

    if (isPayloadValid !== true) {
      return response(400, isPayloadValid);
    }

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    const payload = JSON.parse(event.body);
    payload.id = v4();

    const params = {
      id: payload.id,
      username: payload.username,
      email: payload.email,
      birthdate: payload.birthdate,
    };

    console.log("payload", payload);
    console.log("params", params);

    await dynamodb
      .put({
        TableName: "Users",
        Item: params,
      })
      .promise();

    return response(201, { message: "User created", item: params });
  } catch (error) {
    console.log("addUser:", error);
    return response(500, { message: "An error occurred" });
  }
};
