const { v4 } = require("uuid");
const AWS = require("aws-sdk");
const payloadValidator = require("../../helpers/payloadValidator");
const response = require("../../helpers/response");
const getUser = require("../../libs/getUser");

const schema = require("./schema");

module.exports.updateUser = async (event) => {
  try {
    const { id } = event.pathParameters;

    console.log("updateUser id", id);

    if (id === null || id === undefined) {
      return response(400, {
        error: "Invalid or no queryString parameter defined.",
        message: "No userIds specified.",
      });
    }

    const user = await getUser(id);

    if (user === null || user === undefined) {
      return response(404, {
        error: "userID was invalid",
        message: "userID was not found in the database",
      });
    }

    const isPayloadValid = payloadValidator(schema, event.body);

    if (isPayloadValid !== true) {
      return response(400, isPayloadValid);
    }

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    const { username } = JSON.parse(event.body);

    await dynamodb
      .update({
        TableName: "Users",
        Key: { id },
        UpdateExpression: "set username = :username",
        ExpressionAttributeValues: {
          ":username": username,
        },
        ReturnValues: "ALL_NEW",
      })
      .promise();

    return response(200, { message: "User updated" });
  } catch (error) {
    console.log("updateUser:", error);
    return response(500, { message: "An error occurred" });
  }
};
