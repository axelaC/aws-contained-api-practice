module.exports = {
  type: "object",
  additionalProperties: false,
  required: ["username"],
  properties: {
    username: {
      type: "string",
      maxLength: 10,
      errorMessage:
        '"username" should be string with maximum 10 character length',
    },
  },
  errorMessage: {
    required: {
      username: '"username" is required',
    },
    additionalProperties: "Invalid property",
  },
};
