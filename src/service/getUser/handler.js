const AWS = require("aws-sdk");
const response = require("../../helpers/response");
const getUser = require("../../libs/getUser");

module.exports.getUser = async (event) => {
  try {
    const { id } = event.pathParameters;

    if (id === null || id === undefined) {
      return response(400, {
        error: "Invalid or no queryString parameter defined.",
        message: "No itemIds specified.",
      });
    }

    const user = await getUser(id);

    console.log("getUser", user);

    if (user === null || user === undefined) {
      return response(404, {
        error: "userID was invalid",
        message: "userID was not found in the database",
      });
    }

    return response(200, user);
  } catch (error) {
    console.log("getUser:", error);
    return response(500, { message: "An error occurred" });
  }
};
