const AWS = require("aws-sdk");
const response = require("../../helpers/response");
const getUser = require("../../libs/getUser");

module.exports.deleteUser = async (event) => {
  try {
    const { id } = event.pathParameters;

    if (id === null || id === undefined) {
      return response(400, {
        error: "Invalid or no queryString parameter defined.",
        message: "No itemIds specified.",
      });
    }

    const user = await getUser(id);

    if (user === null || user === undefined) {
      return response(400, {
        error: "userID was invalid",
        message: "userID was not found in the database",
      });
    }

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    const result = await dynamodb
      .delete({
        TableName: "Users",
        Key: { id },
      })
      .promise();

    return response(200, { message: "User deleted" });
  } catch (error) {
    console.log("deleteUser:", error);
    return response(500, { message: "An error occurred" });
  }
};
