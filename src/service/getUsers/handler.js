const AWS = require("aws-sdk");
const response = require("../../helpers/response");

module.exports.getUsers = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    let users;
    const results = await dynamodb.scan({ TableName: "Users" }).promise();
    users = results.Items;
    console.log("getUsers", users);

    if (users === []) {
      return response(404, {
        error: "Users table is empty",
        message: "No users found",
      });
    }

    return response(200, users);
  } catch (error) {
    console.log("getUsers:", error);
    return response(500, { message: "An error occurred" });
  }
};
