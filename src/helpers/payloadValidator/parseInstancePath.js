module.exports = (error) => {
  const params = error.params;
  const instancePath = error.instancePath;

  if (instancePath === "") {
    return params[mapParams(error)];
  }

  if (instancePath.substring(1).split("/").length === 1) {
    return `${instancePath.substring(1)}/${params[mapParams(error)]}`;
  }

  return instancePath.substring(1);
};

function mapParams(error) {
  const params = error.params;
  return Object.keys(params)[0];
}
