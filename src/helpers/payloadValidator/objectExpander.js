module.exports = (str, value, obj) => {
  const items = str.split("/");

  for (let i = 0; i < items.length - 1; i++) {
    if (obj[items[i]] === undefined) {
      obj[items[i]] = {};
    }

    obj = obj[items[i]];
  }

  obj[items[items.length - 1]] = value;

  return obj;
};
