const Ajv = require("ajv").default;
const errorMessage = require("ajv-errors");
const addFormats = require("ajv-formats");
const objectExpander = require("./objectExpander");
const parseInstancePath = require("./parseInstancePath");

module.exports = (schema, payload, options = {}) => {
  if (
    payload === null ||
    payload === undefined ||
    Object.keys(payload).length === 0
  ) {
    return {
      error: "Invalid payload request",
      message: "Request body is empty",
    };
  }

  try {
    if (typeof payload === "string") {
      JSON.parse(payload);
    }
  } catch (error) {
    return {
      error: "Invalid payload request",
      message: "Is not a valid object",
    };
  }

  const parsedPayload =
    typeof payload === "string" ? JSON.parse(payload) : payload;

  const ajv = new Ajv({
    allErrors: true,
    ...options,
  });

  addFormats(ajv, { formats: ["date", "email"], keywords: true });

  errorMessage(ajv, { keepErrors: true });

  const validate = ajv.compile(schema);
  const valid = validate(parsedPayload);

  if (valid) {
    return valid;
  }

  const errors =
    validate.errors &&
    validate.errors
      .filter((error) => !error.emUsed)
      .map((error) => {
        if (error.params.errors !== undefined) {
          error.params = error.params.errors[0].params;
        }

        return error;
      });

  const errorObj = {};

  for (let i = 0; i < errors.length; i++) {
    const error = errors[i];
    const instancePath = parseInstancePath(error);

    objectExpander(instancePath, error.message, errorObj);
  }

  return {
    error: "Invalid payload request",
    messages: errorObj,
  };
};
