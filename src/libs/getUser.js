const db = require("./db");

module.exports = async (id) => {
  try {
    const params = {
      TableName: "Users",
      Key: {
        id: id,
      },
    };

    const { Item } = await db.get(params).promise();

    console.log("getUser", Item);
    return Item;
  } catch (error) {
    console.log("libs/getUser:ERROR", error);
    throw new Error(error);
  }
};
